﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SampleDB.Migrations
{
    public partial class Baseline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblModules",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ModuleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblModules", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TblRoleGroup",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RoleGroupName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblRoleGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TblRoleGroupAccess",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CanRead = table.Column<bool>(nullable: false),
                    CanWrite = table.Column<bool>(nullable: false),
                    FullAccess = table.Column<bool>(nullable: false),
                    MooduleId = table.Column<Guid>(nullable: false),
                    RoleGroupId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblRoleGroupAccess", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TblUserAccount",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    RoleGroupId = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblUserAccount", x => x.UserId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblModules");

            migrationBuilder.DropTable(
                name: "TblRoleGroup");

            migrationBuilder.DropTable(
                name: "TblRoleGroupAccess");

            migrationBuilder.DropTable(
                name: "TblUserAccount");
        }
    }
}
