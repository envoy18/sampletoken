﻿using Microsoft.EntityFrameworkCore;
using SampleDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.DBContext
{
    public class SampleContext : DbContext
    {
        public SampleContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<UserAccount> TblUserAccount { get; set; }

        public DbSet<RoleGroup> TblRoleGroup { get; set; }

        public DbSet<RoleGroupAccess> TblRoleGroupAccess { get; set; }

        public DbSet<Module> TblModules { get; set; }
        
        public DbSet<AccessToken> TblAccessTokens { get; set; }
    }
}
