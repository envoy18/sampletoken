﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models
{
    public class RoleGroupAccess : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        public Guid RoleGroupId { get; set; }

        public Guid MooduleId { get; set; }

        public bool CanRead { get; set; }

        public bool CanWrite { get; set; }

        public bool FullAccess { get; set; }
    }
}
