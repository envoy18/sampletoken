﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models.UIModels
{
    public class CredentialAndRoles
    {
        public string Jwt { get; set; }

        public List<RoleGroupAccess> Roles { get; set; }
    }
}
