﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models
{
    public class AccessToken : BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; } 

        public Guid Token { get; set; } //eto ung ggmtin ntin for SessionToken

        public Guid UserId { get; set; }

        public DateTime ExpiryDate { get; set; }


    }
}
