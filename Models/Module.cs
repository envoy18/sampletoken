﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models
{
    public class Module : BaseModel
    {
        [Key]
        public Guid Id { get; set; }

        public string ModuleName { get; set; }
    }
}
