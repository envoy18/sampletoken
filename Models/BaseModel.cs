﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models
{
    public class BaseModel
    {
        public DateTime DateCreated;

        public string UserCreated;

        public DateTime DateUpdated;

        public string UserUpdated;
    }
}
