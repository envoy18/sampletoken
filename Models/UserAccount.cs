﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Models
{
    public class UserAccount : BaseModel
    {
        [Key]
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public Guid RoleGroupId { get; set; }


    }
}
