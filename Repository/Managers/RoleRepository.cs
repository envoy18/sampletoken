﻿using SampleDB.DBContext;
using SampleDB.Models;
using SampleDB.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Repository.Managers
{
    public class RoleRepository : IRoleGroupAccessRepository
    {
        private SampleContext _context;

        public RoleRepository(SampleContext dbContext)
        {
            _context = dbContext;
        }

        public List<RoleGroupAccess> GetRoleGroupAccessByUserId(Guid roleGroupId)
        {
            return _context.TblRoleGroupAccess.Where(x => x.RoleGroupId == roleGroupId).ToList();
        }
        
    }
}
