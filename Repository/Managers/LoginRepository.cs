﻿using SampleDB.DBContext;
using SampleDB.Models;
using SampleDB.Models.UIModels;
using SampleDB.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Repository.Managers
{
    public class LoginRepository : ILoginRepository
    {
        private SampleContext _context;

        public LoginRepository(SampleContext dbContext)
        {
            _context = dbContext;
        }

        public UserAccount ValidateLogin(LoginCredential userAccoount)
        {
           return _context.TblUserAccount.Where(x => x.UserName == userAccoount.UserName && x.Password == userAccoount.Password).FirstOrDefault();
        }
    }
}
