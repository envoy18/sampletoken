﻿using SampleDB.DBContext;
using SampleDB.Models;
using SampleDB.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Repository.Managers
{
    public class AccessTokenRepository : IAccessTokenRepository
    {

        private SampleContext _context;

        public AccessTokenRepository(SampleContext dbContext)
        {
            _context = dbContext;
        }

        public int CreateToken(AccessToken accessToken)
        {
            return _context.TblAccessTokens.Add(accessToken).GetHashCode();
        }

        public AccessToken GetAccessTokenByUserId(Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}
