﻿using SampleDB.Models;
using SampleDB.Models.UIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Repository.Repositories
{
    public interface ILoginRepository
    {
        UserAccount ValidateLogin(LoginCredential userAccoount);
    }
}
