﻿using SampleDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleDB.Repository.Repositories 
{
    public interface IAccessTokenRepository
    {
        int CreateToken(AccessToken accessToken);

        AccessToken GetAccessTokenByUserId(Guid userId);
    }
}
