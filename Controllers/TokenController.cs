﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace SampleDB.Controllers
{
    [Produces("application/json")]
    [Route("api/Token")]
    public class TokenController : Controller
    {
        [HttpGet]
        [Route("SampleToken")]
        public string SampleToken()
        {
            string ret = "";

            string SampleSessionKey = "bf14b3b3-df25-4727-a01d-d589ba8f9ee8"; //GUID na cnreate ntin for session

            var sampleSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SampleSessionKey));

            var sampleCredentials = new SigningCredentials(sampleSecurityKey, SecurityAlgorithms.HmacSha256);

            var header = new JwtHeader(sampleCredentials);

            // dto dpat ung roles ng user
            var payload = new JwtPayload
            {
                { "user ", "TestUser"},
                { "password", "pass1234"},
            };

            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();
            
            ret = handler.WriteToken(secToken);

            return ret;
        }
    }
}