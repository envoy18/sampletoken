﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SampleDB.Models;
using SampleDB.Models.UIModels;
using SampleDB.Repository.Repositories;


namespace SampleDB.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        #region FIELDS

        private readonly ILoginRepository _loginRepository;
        private readonly IAccessTokenRepository _accessTokenRepository;
        private readonly IRoleGroupAccessRepository _roleGroupAccessRepository;

        private CredentialAndRoles _returnData;

        #endregion

        #region PROPERTIES
        
        public CredentialAndRoles ReturnData
        {
            get { return _returnData; }
            set { _returnData = value; }
        }

        #endregion

        #region CONSTRUCTOR
        public LoginController(ILoginRepository loginRepository,
                               IAccessTokenRepository accessTokenRepository,
                               IRoleGroupAccessRepository roleGroupAccessRepository)
        {
            _loginRepository = loginRepository;
            _accessTokenRepository = accessTokenRepository;
            _roleGroupAccessRepository = roleGroupAccessRepository;
        }
        #endregion

        #region METHODS
        [HttpPost]
        [Route("ValidateCredential")]
        [EnableCors("AllowAnyOrigin")]
        public CredentialAndRoles ValidatedCredential(LoginCredential loginCredential)
        {
            UserAccount userAccount = _loginRepository.ValidateLogin(loginCredential);

            if(userAccount != null)
            {
                AccessToken accessToken = new AccessToken()
                {
                    UserId = userAccount.UserId,
                    Token = Guid.NewGuid(),
                    UserCreated = userAccount.UserName,
                    DateCreated = DateTime.Now,
                    ExpiryDate = DateTime.Now.AddDays(1)
                };
                _accessTokenRepository.CreateToken(accessToken);

                List<RoleGroupAccess> roleGroupAccesses = _roleGroupAccessRepository.GetRoleGroupAccessByUserId(userAccount.RoleGroupId);

                string SampleSessionKey = accessToken.Token.ToString(); //GUID na cnreate ntin for session
                var sampleSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SampleSessionKey));
                var sampleCredentials = new SigningCredentials(sampleSecurityKey, SecurityAlgorithms.HmacSha256);
                var header = new JwtHeader(sampleCredentials);

                // dto dpat ung roles ng user
                var payload = new JwtPayload
                {
                    { "user ", userAccount.UserId},
                    { "token", accessToken.Token},
                    { "role", userAccount.RoleGroupId},
                };
                var secToken = new JwtSecurityToken(header, payload);
                var handler = new JwtSecurityTokenHandler();
                

                ReturnData = new CredentialAndRoles()
                {
                    Jwt = handler.WriteToken(secToken),
                    Roles = roleGroupAccesses
                };
            }

            return ReturnData;
        }

        [HttpGet]
        [Route("Integer")]
        [EnableCors("AllowAnyOrigin")]
        public List<int> Integer()
        {
            List<int> ret = new List<int>() { 1, 2, 3, 4 };

            return ret;
        }
        #endregion

    }
}